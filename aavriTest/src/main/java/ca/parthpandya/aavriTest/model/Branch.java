package ca.parthpandya.aavriTest.model;

public class Branch {
		
	//TODO.PP.Lombok @Getter @Setter should clean up this code
	private String branchName;
	private float latitude;
	private float longitude;
	private String streetAddress;
	private String city;
	private String countrySubDivision;
	private String country;
	private String postCode;
	
	
	public Branch() {}
	
	// Full arg constructor
	public Branch(String branchName, float latitude, float longitude, String streetAddress, String city,
			String countrySubDivision, String country, String postCode) {
		this.branchName = branchName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.streetAddress = streetAddress;
		this.city = city;
		this.countrySubDivision = countrySubDivision;
		this.country = country;
		this.postCode = postCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountrySubDivision() {
		return countrySubDivision;
	}
	public void setCountrySubDivision(String countrySubDivision) {
		this.countrySubDivision = countrySubDivision;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branchName == null) ? 0 : branchName.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((countrySubDivision == null) ? 0 : countrySubDivision.hashCode());
		result = prime * result + Float.floatToIntBits(latitude);
		result = prime * result + Float.floatToIntBits(longitude);
		result = prime * result + ((postCode == null) ? 0 : postCode.hashCode());
		result = prime * result + ((streetAddress == null) ? 0 : streetAddress.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (branchName == null) {
			if (other.branchName != null)
				return false;
		} else if (!branchName.equals(other.branchName))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (countrySubDivision == null) {
			if (other.countrySubDivision != null)
				return false;
		} else if (!countrySubDivision.equals(other.countrySubDivision))
			return false;
		if (Float.floatToIntBits(latitude) != Float.floatToIntBits(other.latitude))
			return false;
		if (Float.floatToIntBits(longitude) != Float.floatToIntBits(other.longitude))
			return false;
		if (postCode == null) {
			if (other.postCode != null)
				return false;
		} else if (!postCode.equals(other.postCode))
			return false;
		if (streetAddress == null) {
			if (other.streetAddress != null)
				return false;
		} else if (!streetAddress.equals(other.streetAddress))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "branch [branchName=" + branchName + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", streetAddress=" + streetAddress + ", city=" + city + ", countrySubDivision=" + countrySubDivision
				+ ", country=" + country + ", postCode=" + postCode + "]";
	}
		
}
