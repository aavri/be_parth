package ca.parthpandya.aavriTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AavriTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(AavriTestApplication.class, args);
	}
}
