package ca.parthpandya.aavriTest.service;

import java.util.List;

import ca.parthpandya.aavriTest.model.Branch;

public interface IBankService {

	/**
	 * Returns list of all the bank branches
	 * Implementation uses Apache Camel's route which calls Halifax public Bank API, returned JSON converted into Branch POJO List.
	 * 
	 * @return List<Branch>
	 * @throws Exception 
	 */
	List<Branch> findAllBranches() throws Exception;
	
	/**
	 * Returns list of all the bank branches filtered by city name.
	 * Implementation uses WebTarget which calls Halifax public Bank API, returned JSON converted into Branch POJO & filtered through JSONObject.
	 *  
	 * @param cityName
	 * @return List<Branch>
	 */
	List<Branch> findAllBranchesByCity(String cityName);
}
