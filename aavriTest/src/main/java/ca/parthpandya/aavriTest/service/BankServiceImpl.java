package ca.parthpandya.aavriTest.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.camel.ProducerTemplate;
import org.apache.commons.codec.binary.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.parthpandya.aavriTest.ApplicationConstants;
import ca.parthpandya.aavriTest.model.Branch;

@Service("bankService")
public class BankServiceImpl implements IBankService {

	private static List<Branch> branches = null;
	public static final Logger logger = LoggerFactory.getLogger(BankServiceImpl.class);

	// static class branchList extends ArrayList<Branch> { };

	@Autowired
	ProducerTemplate producerTemplate;

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> findAllBranches() throws Exception {
		logger.info("findAllBranches called");

		Object jsonString = getProducerTemplate().requestBody(ApplicationConstants.DIRECT_FROM, null, Object.class);
		//Type typeOfT = new TypeToken<Collection<Branch>>() {}.getType();

		// TODO.PP.Revise to use camel in built POJO conversion
		/*Gson gson = new GsonBuilder().create();
		branches = gson.fromJson((String) jsonString, typeOfT);*/

		InputStream inputStream = new ByteArrayInputStream(((String) jsonString).getBytes(Charset.forName("UTF-8")));
		BufferedReader bR = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

					String line = "";

			StringBuilder responseStrBuilder = new StringBuilder();
			while ((line = bR.readLine()) != null) {
				responseStrBuilder.append(line);
			}
		
		JSONObject jsonObj = new JSONObject(responseStrBuilder.toString());

		JSONArray jsonDataArr = (JSONArray) jsonObj.get("data");
		JSONArray brandArray = (JSONArray) jsonDataArr.getJSONObject(0).get("Brand");
		JSONArray branchArray = (JSONArray) brandArray.getJSONObject(0).get("Branch");

		branches = new ArrayList<Branch>();
		if (branchArray != null) {
			int len = branchArray.length();
			for (int i = 0; i < len; i++) {
				JSONObject obj = (JSONObject) branchArray.get(i);
				JSONObject postalObj = (JSONObject) obj.get("PostalAddress");
				logger.info(postalObj.toString());

				if (postalObj != null && postalObj.has("TownName")) {
					Branch b = new Branch(obj.getString("Name"),
							postalObj.getJSONObject("GeoLocation").getJSONObject("GeographicCoordinates").getFloat("Latitude"),
							postalObj.getJSONObject("GeoLocation").getJSONObject("GeographicCoordinates").getFloat("Longitude"),
							postalObj.getJSONArray("AddressLine").get(0).toString(),
							postalObj.getString("TownName"),
							postalObj.getJSONArray("CountrySubDivision").get(0).toString(),
							postalObj.getString("Country"), postalObj.getString("PostCode"));
					branches.add(b);
				} else {
					Branch b = new Branch("",
							postalObj.getJSONObject("GeoLocation").getJSONObject("GeographicCoordinates").getFloat("Latitude"),
							postalObj.getJSONObject("GeoLocation").getJSONObject("GeographicCoordinates").getFloat("Longitude"),
							postalObj.getJSONArray("AddressLine").get(0).toString(),
							"",
							"",
							postalObj.getString("Country"), 
							postalObj.getString("PostCode"));
				}
			}
		}
		if (branches.isEmpty()) {
			branches = new ArrayList<Branch>();
		}
		return branches;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> findAllBranchesByCity(final String cityName) {
		System.out.println("findAllBranchesByCity called");

		final Client client = ClientBuilder.newClient();
		try {
			WebTarget webTarget = client
					.target(ApplicationConstants.HALIFAX_OPENBANK_TARGET_URI + ApplicationConstants.BRANCHES_ENDPOINT);
			InputStream inputStream = webTarget
					.request(MediaType.valueOf("application/prs.openbanking.opendata.v2.1.json"))
					.get(InputStream.class);

			BufferedReader bR;
			try {
				bR = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

				String line = "";

				StringBuilder responseStrBuilder = new StringBuilder();
				while ((line = bR.readLine()) != null) {
					responseStrBuilder.append(line);
				}
				JSONObject jsonObj = new JSONObject(responseStrBuilder.toString());

				JSONArray jsonDataArr = (JSONArray) jsonObj.get("data");
				JSONArray brandArray = (JSONArray) jsonDataArr.getJSONObject(0).get("Brand");
				JSONArray branchArray = (JSONArray) brandArray.getJSONObject(0).get("Branch");

				branches = new ArrayList<Branch>();
				if (branchArray != null) {
					int len = branchArray.length();
					for (int i = 0; i < len; i++) {
						JSONObject obj = (JSONObject) branchArray.get(i);
						JSONObject postalObj = (JSONObject) obj.get("PostalAddress");
						logger.info(postalObj.toString());

						if (postalObj != null && postalObj.has("TownName")
								&& StringUtils.equals(postalObj.getString("TownName"), cityName.toUpperCase())) {
							Branch b = new Branch(obj.getString("Name"),
									postalObj.getJSONObject("GeoLocation").getJSONObject("GeographicCoordinates")
											.getFloat("Latitude"),
									postalObj.getJSONObject("GeoLocation").getJSONObject("GeographicCoordinates")
											.getFloat("Longitude"),
									postalObj.getJSONArray("AddressLine").get(0).toString(),
									postalObj.getString("TownName"),
									postalObj.getJSONArray("CountrySubDivision").get(0).toString(),
									postalObj.getString("Country"), postalObj.getString("PostCode"));
							branches.add(b);
						}
					}
				}

				System.out.println("Branch size" + branches.size());

				/*
				 * Gson gson = new Gson(); Type type = new
				 * TypeToken<List<Branch>>(){}.getType(); List<Branch> json =
				 * gson.fromJson(branchJson.toString(), type);
				 */

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
			client.close();
		}

		if (branches.isEmpty()) {
			branches = new ArrayList<Branch>();
		}

		return branches;
	}

	public ProducerTemplate getProducerTemplate() {
		return producerTemplate;
	}

}
