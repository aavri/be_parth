package ca.parthpandya.aavriTest.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.parthpandya.aavriTest.ApplicationConstants;
import ca.parthpandya.aavriTest.service.IBankService;

@Component
public class HalifaxOpenBankRoute extends RouteBuilder {

	@Autowired
	private IBankService bankService;

	@Override
	public void configure() throws Exception {
		
		// streamCaching is for fix ref: https://stackoverflow.com/questions/41016782/apachecamel-request-body-gets-lost-after-using-log-component
		// routes that implement the REST service by invoking HTTP call to Halifax OpenBank API
		from(ApplicationConstants.DIRECT_FROM)
			.startupOrder(100)
			.streamCaching()  // Fix for body disappear after log component used. 
			.routeId(ApplicationConstants.ROUTE_ID)
			.setHeader(Exchange.HTTP_METHOD, org.apache.camel.component.http.HttpMethods.GET)
				.log("From - Headers:: ${headers}")
				.log("From - Message Body :: ${body}")
				.log("Exchange :: ${exchange}")
			.to(ApplicationConstants.HALIFAX_OPENBANK_TARGET_URI  + ApplicationConstants.BRANCHES_ENDPOINT)
				.log("To - Headers:: ${headers}")
				.log("To - Message Body :: ${body}")
			.process(new Processor() {
			    public void process(Exchange exchange) throws Exception {
			            // log caught exception
			        	Exception exception = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
			              
			        	// log response body & code
			            String responseBody = exchange.getIn().getBody(String.class);
			     		int responseCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class).intValue();
			     		
			     		exchange.getOut().setBody(responseBody);
			     		System.out.println("end processing route !! \nResponse: " + responseBody +" HTTP ResponseCode: " + responseCode);
			         }
			    })
			//.convertBodyTo(String.class)
			//.setBody().body()
			//.bean(bankService.getClass(), ApplicationConstants.TO_BEAN_METHOD)
			.end();
	}
}