package ca.parthpandya.aavriTest.controller;

import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ca.parthpandya.aavriTest.model.Branch;
import ca.parthpandya.aavriTest.service.IBankService;
 
@RestController
public class RESTController {
 
    public static final Logger logger = LoggerFactory.getLogger(RESTController.class);
 
    @Autowired
    IBankService bankService; 
   
    // -------------------Retrieve all Branches-----------------------------
 
    @RequestMapping(value = "/branches", method = RequestMethod.GET)
    public ResponseEntity<List<Branch>> listAllBranches() {
    	logger.info("Fetching all branches");
    	
    	List<Branch> branches = null;
		try {
			branches = bankService.findAllBranches();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if (branches.isEmpty()) {
        	return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        
		return new ResponseEntity<List<Branch>>(branches, HttpStatus.OK);
    }
 
    // -------------------Retrieve all Branches by City----------------------
 
    @RequestMapping(value = "/branches/{city}", method = RequestMethod.GET)
    public ResponseEntity<List<Branch>> listAllBranchesByCity(@PathVariable("city") String city)  {
    	logger.info("Fetching branches by city {}"+ city);
        
    	if (StringUtils.isEmpty(city)) {
    		return new ResponseEntity(HttpStatus.BAD_REQUEST);
    	}
    	
        List<Branch> branches = bankService.findAllBranchesByCity(city);
        if (branches == null || branches.isEmpty()) {
            logger.error("Branches by city not found.", city);
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        
        return new ResponseEntity<List<Branch>>(branches, HttpStatus.OK);
    }
  
}
