package ca.parthpandya.aavriTest;

/**
 * Holds all the constants used in the routes
 * @author parth.pandya
 *
 */
public final class ApplicationConstants {
	public static final String DIRECT_FROM = "direct:branches";
    public static final String ROUTE_ID = "HalifaxOpenBankRoute:branches";
    public static final String TO_BEAN_METHOD = "findAllBranches";
    
    
    //TODO.PP.Move to properties file
    public static final String HALIFAX_OPENBANK_TARGET_URI = "https://api.halifax.co.uk/open-banking/v2.2";
    public static final String BRANCHES_ENDPOINT ="/branches";

    private ApplicationConstants() {

    }
}
